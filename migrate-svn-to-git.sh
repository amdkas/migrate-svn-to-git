#!/bin/bash
if [[ "$#" -ne 3 ]]; then
    echo "This script needs 3 parameters : the SVN URL (directory above the trunk/tags/branches), the git URL, and a working directory"
    echo "It can be run from Git Bash (on Windows) or from Linux (tested on CentOS 7, but should work on many other systems)"
    echo "Example :"
    echo "  ./migrate-svn-to-git.sh https://svnserver/repo/directory-above-trunk https://gitserver/namespace/reponame.git /d/temp/svn-to-git-migrations/reponame"
    exit 2
fi
URL_SVN=$1
URL_GIT=$2
PATH_TEMP=$3

INITIAL_PATH=$(pwd)

echo "Migration from SVN repo $URL_SVN to git repo $URL_GIT, using temporary working directory $PATH_TEMP"

echo "Cleanup"
mkdir -p $PATH_TEMP
rm -rf $PATH_TEMP/temp
rm -rf $PATH_TEMP/new-bare.git
rm -rf $PATH_TEMP/clone-from-git-server

echo "Cloning SVN repo $URL_SVN"
git svn clone $URL_SVN --no-metadata -A final-authors.txt --stdlayout --prefix "" $PATH_TEMP/temp
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Converting svn:ignore to .gitignore"
cd $PATH_TEMP/temp
git svn show-ignore  --id=trunk > .gitignore
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Creating bare repo"
git init --bare $PATH_TEMP/new-bare.git
cd $PATH_TEMP/new-bare.git
git symbolic-ref HEAD refs/heads/trunk
if [[ $? -ne 0 ]] ; then
    exit 1
fi

cd $PATH_TEMP/temp
git remote add bare $PATH_TEMP/new-bare.git
git config remote.bare.push 'refs/remotes/*:refs/heads/*'
git push bare
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Renaming trunk branch to master"
cd $PATH_TEMP/new-bare.git
git branch -m trunk master
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Cleaning branches and tags"
cd $PATH_TEMP/new-bare.git
git for-each-ref --format='%(refname)' refs/heads/tags |
cut -d / -f 4 |
while read ref
do
  git tag "$ref" "refs/heads/tags/$ref";
  if [[ $? -ne 0 ]] ; then
      exit 1
  fi
  git branch -D "tags/$ref";
  if [[ $? -ne 0 ]] ; then
      exit 1
  fi
done

echo "Sending the data to git server"
cd $PATH_TEMP/new-bare.git
git remote add origin $URL_GIT
git push --set-upstream origin --all
if [[ $? -ne 0 ]] ; then
    exit 1
fi
git push --set-upstream origin --tags
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Normalising line endings"
git clone $URL_GIT $PATH_TEMP/clone-from-git-server
if [[ $? -ne 0 ]] ; then
    exit 1
fi
cp $INITIAL_PATH/.gitattributes-force-lf-line-endings $PATH_TEMP/clone-from-git-server/.gitattributes
cd $PATH_TEMP/clone-from-git-server
git add . --renormalize
if [[ $? -ne 0 ]] ; then
    exit 1
fi
tail -n+4 .gitattributes >.gitattributes-new
rm .gitattributes
mv .gitattributes-new .gitattributes

git add .
git commit -m "Normalize line endings to LF"
if [[ $? -ne 0 ]] ; then
    exit 1
fi
git push origin master
if [[ $? -ne 0 ]] ; then
    exit 1
fi

echo "Commiting .gitignore"
cp $PATH_TEMP/temp/.gitignore $PATH_TEMP/clone-from-git-server/
git add .gitignore
git commit -m 'Convert svn:ignore properties to .gitignore'
git push origin master

echo "End of of migration"
